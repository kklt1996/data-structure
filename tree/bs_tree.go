package tree

import (
	"fmt"
	"gitee.com/kklt1996/data-structure/queue"
	"gitee.com/kklt1996/data-structure/stack"
	"gitee.com/kklt1996/data-structure/util"
	"strings"
)

/*
	二分搜索树是二叉树,因为其中序列遍历的有序性也称二分搜索树为排序树

	二分搜索树的每隔节点的值:
		大于其左子节点的所有节点的值
		小于其右子节点的所有节点的值
	为了满足此特性,二分搜索树中不允许有相等的元素

	每一棵子树也是二分搜索树

	存入二分搜索树中的节点必须是可以比较的,想要存入基础类型就必须给基础类型实现
	 common.CompareAble接口
*/
type node struct {
	key, value  interface{}
	left, right *node
}

/*
	二分搜索树

*/
type BsTree struct {
	// 二分搜索树的根节点
	root *node
	// 元素元素个数
	size int

	/*
		使用comparator比较堆中元素大小,实现common.CompareAble接口和传入比较器形式二选一
		1 thisKey＞compareKey
		0 thisKey=compareKey
		-1 thisKey<compareKey
	*/
	comparator func(thisKey interface{}, compareKey interface{}) int
}

/*
	O(1)
	获取元素个数
*/
func (tree BsTree) GetSize() int {
	return tree.size
}

/*
	O(1)
	判断是否为空
*/
func (tree BsTree) IsEmpty() bool {
	return tree.size == 0
}

/*
	O(h)
	添加元素
*/
func (tree *BsTree) Add(key interface{}, value interface{}) {
	tree.root = tree.add(tree.root, key, value)
}

/*
	向以node为根节点的二分搜索树中插入元素e
	返回以node为根节点的二分搜索树插入新节点之后以的根
	空节点也是一个二叉树
*/
func (tree *BsTree) add(root *node, key interface{}, value interface{}) *node {
	// 找到满足条件,且为空的位置,就插入元素返回根节点,也就是根节点赋值为新增节点的地址
	if root == nil {
		tree.size++
		// 这就是递归的临界点,在最小的树中找到了插入元素的位置
		return &node{key: key, value: value}
	} else {
		// 要添加的元素和当前根节点比较大小
		i := tree.compare(key, root.key)
		if i < 0 {
			root.left = tree.add(root.left, key, value)
		} else if i > 0 {
			root.right = tree.add(root.right, key, value)
		} else if i == 0 {
			root.value = value
		}
		// 当前节点不为空,则根节点就是当前节点,当前节点无变化
		return root
	}
}

/*
	O(h)
	查询二分搜索树中是否包含某个元素
*/
func (tree BsTree) Contains(key interface{}) bool {
	return tree.contains(tree.root, key)
}

/*
	以node为根的二分搜索树是否包含元素value
*/
func (tree BsTree) contains(root *node, key interface{}) bool {
	// 当查找到节点为空的时候,表明叶子节点都不等于value.那一定是不包含了
	if root == nil {
		return false
	}
	i := tree.compare(key, root.key)
	if i == 0 {
		// 找到表明包含
		return true
	} else if i < 0 {
		// 小于当前节点就去左孩子去继续找
		return tree.contains(root.left, key)
	} else {
		// 大于当前节点就去右边孩子去继续找
		return tree.contains(root.right, key)
	}
}

/*
	查询key对应的value
*/
func (tree BsTree) Get(key interface{}) interface{} {
	return tree.get(tree.root, key)
}

func (tree BsTree) get(root *node, key interface{}) interface{} {
	if root == nil {
		return nil
	}
	i := tree.compare(key, root.key)
	if i == 0 {
		return root.value
	} else if i < 0 {
		return tree.get(root.left, key)
	} else {
		return tree.get(root.right, key)
	}
}

/*
	前序遍历,进行一些操作
*/
func (tree BsTree) PreOrder(operatorFunc func(key interface{}, value interface{}) bool) {
	tree.preOrder(tree.root, operatorFunc)
}

/*
	以某个节点为根节点进行前序列遍历
*/
func (tree BsTree) preOrder(root *node, operatorFunc func(key interface{}, value interface{}) bool) {
	// 当没有node的时候表示结束
	if root == nil {
		return
	}
	if operatorFunc(root.key, root.value) {
		return
	}
	tree.preOrder(root.left, operatorFunc)
	tree.preOrder(root.right, operatorFunc)
}

/*
	二分搜索树的中序遍历,中序遍历的结果是升序排序的
*/
func (tree BsTree) InOrder(operatorFunc func(key interface{}, value interface{}) bool) {
	tree.inOrder(tree.root, operatorFunc)
}

/*
	以某个node为根节点进行中序列遍历
*/
func (tree BsTree) inOrder(root *node, operatorFunc func(key interface{}, value interface{}) bool) {
	// 当没有node的时候表示结束
	if root == nil {
		return
	}
	tree.inOrder(root.left, operatorFunc)
	if operatorFunc(root.key, root.value) {
		return
	}
	tree.inOrder(root.right, operatorFunc)
}

/*
	二分搜索树的后序遍历
*/
func (tree BsTree) PostOrder(operatorFunc func(key interface{}, value interface{}) bool) {
	tree.postOrder(tree.root, operatorFunc)
}

/*
	以某个node为根节点进行后序列遍历
*/
func (tree BsTree) postOrder(root *node, operatorFunc func(key interface{}, value interface{}) bool) {
	// 当没有node的时候表示结束
	if root == nil {
		return
	}
	tree.postOrder(root.left, operatorFunc)
	tree.postOrder(root.right, operatorFunc)
	if operatorFunc(root.key, root.value) {
		return
	}
}

/*
	二分搜索树的前序遍历非递归实现
*/
func (tree BsTree) PreOrderNR(operatorFunc func(key interface{}, value interface{}) bool) {
	var stk stack.Stack = stack.CreateLinkedListStack()
	if tree.root != nil {
		stk.Push(tree.root)
	}

	for !stk.IsEmpty() {
		pop, _ := stk.Pop()
		node := pop.(*node)
		if !operatorFunc(node.key, node.value) {
			return
		}
		if node.right != nil {
			stk.Push(node.right)
		}
		if node.left != nil {
			stk.Push(node.left)
		}
	}

}

/*
	借助队列实现二分搜索树的层序遍历
*/
func (tree BsTree) LevelOrder(operatorFunc func(key interface{}, value interface{}) bool) {
	var q queue.Queue = queue.CreateLinkedListQueue()
	if tree.root != nil {
		q.Enqueue(tree.root)
	}
	for !q.IsEmpty() {
		// 遍历当前层节点
		dequeue, _ := q.Dequeue()
		node := dequeue.(*node)
		// 搜索结束,直接返回结果
		if operatorFunc(node.key, node.value) {
			return
		}

		// 下一层节点入队,当前层节点遍历完成后就会遍历下一层节点
		if node.left != nil {
			q.Enqueue(node.left)
		}
		if node.right != nil {
			q.Enqueue(node.right)
		}
	}
}

/*
	O(h)
	查找二分搜索树中最小的元素
*/
func (tree BsTree) Minimum() (interface{}, interface{}, error) {
	if tree.IsEmpty() {
		return nil, nil, BstIsEmptyError{}
	}
	minimumNode := tree.minimum(tree.root)
	return minimumNode.key, minimumNode.value, nil
}

/*
	递归查找最小元素的节点,也就是微分搜索树最左边的节点
*/
func (tree BsTree) minimum(node *node) *node {
	if node.left == nil {
		return node
	}
	return tree.minimum(node.left)
}

/*
	O(h)
	查找最小的元素
*/
func (tree BsTree) Maximum() (interface{}, interface{}, error) {
	if tree.IsEmpty() {
		return nil, nil, BstIsEmptyError{}
	}
	maximumNode := tree.maximum(tree.root)
	return maximumNode.key, maximumNode.value, nil
}

/*
	递归查找最大元素的节点,也就是微分搜索树最右边的节点
*/
func (tree BsTree) maximum(node *node) *node {
	if node.right == nil {
		return node
	}
	return tree.maximum(node.right)
}

/*
	O(h)
	删除二分搜索树的最小的节点
*/
func (tree *BsTree) RemoveMinimum() (interface{}, interface{}, error) {
	if tree.IsEmpty() {
		return nil, nil, BstIsEmptyError{}
	}
	newRoot, minimumKey, minimumValue := tree.removeMinimum(tree.root)
	// 更新以 tree.root 为根节点子树的根为新的根
	tree.root = newRoot
	return minimumKey, minimumValue, nil
}

/*
	删除以node为根节点的二分搜索树的最小值
	返回删除最小的元素节点后二分搜索树的根和最小的元素
*/
func (tree *BsTree) removeMinimum(root *node) (*node, interface{}, interface{}) {
	if root.left == nil {
		// 找到最小的元素,获取要返回的右子树
		rightTree := root.right
		// 删除节点和右孩子的关系
		root.right = nil
		// 元素个数-1
		tree.size--
		// 返回最小节点的右子树,和最小节点的值
		return rightTree, root.key, root.value
	} else {
		// 删除左子树最小的元素
		newRoot, minimumKey, minimumValue := tree.removeMinimum(root.left)
		// 更新以 root.left 为根节点子树的根为新的根
		root.left = newRoot
		// 返回原根和删除最小节点的value
		return root, minimumKey, minimumValue
	}
}

/*
	O(h)
	删除二分搜索树的最大的节点
*/
func (tree *BsTree) RemoveMaximum() (interface{}, interface{}, error) {
	if tree.IsEmpty() {
		return nil, nil, BstIsEmptyError{}
	}
	newRoot, maximumKey, maximumValue := tree.removeMaximum(tree.root)
	tree.root = newRoot
	return maximumKey, maximumValue, nil
}

/*
	删除以node为根节点的二分搜索树的最大值的节点
	返回删除最大的元素节点后二分搜索树的根
*/
func (tree *BsTree) removeMaximum(root *node) (*node, interface{}, interface{}) {
	if root.right == nil {
		// 找到最大的元素,获取要返回的左子树
		leftTree := root.left
		// 删除节点和左孩子的关系
		root.left = nil
		// 元素个数-1
		tree.size--
		// 返回最大节点的左子树
		return leftTree, root.key, root.value
	} else {
		// 删除右子树最大的元素
		newRoot, maximumKey, maximumValue := tree.removeMaximum(root.right)
		root.right = newRoot
		return root, maximumKey, maximumValue
	}
}

/*
	O(h)
	删除二分搜索树中任意节点
*/
func (tree *BsTree) RemoveElement(key interface{}) (interface{}, error) {
	if tree.IsEmpty() {
		return nil, BstIsEmptyError{}
	}
	var removeValue interface{}
	tree.root, removeValue = tree.removeElement(tree.root, key)
	return removeValue, nil
}

/*
	删除以某个node为根节点的二分搜索树中的某一个节点
	返回删除指定节点后新二分搜索树的根
*/
func (tree *BsTree) removeElement(root *node, key interface{}) (*node, interface{}) {
	// 根节点为空直接返回
	if root == nil {
		return nil, nil
	}
	i := tree.compare(key, root.key)
	if i == 0 {
		// 递归终止条件
		// 找到需要删除的节点
		if root.left == nil {
			// 左子树为空,那么直接将右节点升级为二分搜索树的根
			rightTree := root.right
			root.right = nil
			tree.size--
			return rightTree, root.value
		}
		if root.right == nil {
			// 右子树为空,那么直接将左子树升级为二分搜索树的根
			leftTree := root.left
			root.left = nil
			tree.size--
			return leftTree, root.value
		}
		// 左右子树都不为空,那么为满足二分搜索树的性质
		// 需要从右子树中删除最小的节点或者从左子树中删除最大的节点
		// 将删除的节点赋值给当前根节点
		rightNewRoot, removeKey, removeValue := tree.removeMinimum(root.right)
		// 接收删除的值
		retRemoveValue := root.value
		root.right = rightNewRoot
		root.key = removeKey
		root.value = removeValue
		return root, retRemoveValue
	} else {
		// 用更小的结果拼凑最终的结果,在子树中删除元素
		var retRemoveValue interface{}
		if i < 0 {
			root.left, retRemoveValue = tree.removeElement(root.left, key)
		} else {
			root.right, retRemoveValue = tree.removeElement(root.right, key)
		}
		// 根不变
		return root, retRemoveValue
	}
}

/*
	获取树的最大高度
*/
func (tree BsTree) MaxDepth() int {
	return tree.maxDepth(tree.root)
}

/*
	获取二分搜索树的最大高度
	返回当前二分搜索树的高度
*/
func (tree BsTree) maxDepth(root *node) int {
	if root == nil {
		// 空节点返回0,不计算高度
		return 0
	} else {
		leftTreeMaxDepth := tree.maxDepth(root.left)
		rightTreeMaxDepth := tree.maxDepth(root.right)
		// 左右子树的最大高度+1,就是当前二分搜索树的最大高度
		// +1 表示每经过一个非空节点，高度+1
		if leftTreeMaxDepth > rightTreeMaxDepth {
			return leftTreeMaxDepth + 1
		} else {
			return rightTreeMaxDepth + 1
		}
	}
}

func (tree BsTree) compare(thisValue interface{}, compareValue interface{}) int {
	if tree.comparator != nil {
		return tree.comparator(thisValue, compareValue)
	} else {
		return util.DefaultComparator(thisValue, compareValue)
	}
}

func (tree BsTree) String() string {
	res := "["
	tree.inOrder(tree.root, func(key interface{}, value interface{}) bool {
		res += fmt.Sprint(key) + ","
		return false
	})
	res = strings.TrimSuffix(res, ",")
	res += "]"
	return res
}

type BstIsEmptyError struct {
}

func (BstIsEmptyError) Error() string {
	return "bst is empty"
}
