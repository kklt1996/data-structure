package tree

import (
	"fmt"
	"testing"
)

func TestCreateUnionFind(t *testing.T) {
	find := CreateUnionQuickFind(10)
	find.UnionElements(1, 4)
	connected := find.IsConnected(0, 4)
	fmt.Println(connected)
}

func TestCreateQuickUnionFind(t *testing.T) {
	find := CreateQuickUnionFind(10)
	find.UnionElements(1, 4)
	connected := find.IsConnected(1, 4)
	if !connected {
		t.Errorf("1 4 is excepted connected but false")
	}
}
