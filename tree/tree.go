package tree

import (
	"gitee.com/kklt1996/data-structure/array"
	"gitee.com/kklt1996/data-structure/common"
)

/*
	创建二分搜索树
*/
func CreateBstTree(comparator func(thisValue interface{}, compareValue interface{}) int) *BsTree {
	return &BsTree{comparator: comparator}
}

/*
	创建一个线段树
*/
func CreateArraySegmentTree(slice array.Array, aggregator func(leftChildValue interface{}, rightChildValue interface{}) interface{}) *ArraySegmentTree {
	data := make([]interface{}, slice.Size(), slice.Size())
	// copy原来的数组
	i := 0
	slice.Iterator(func(iterator common.Iterator) bool {
		v, _ := iterator.Get()
		data[i] = v
		i++
		return false
	})
	// 创建一个存储满二叉树的数组
	tree := make([]interface{}, slice.Size()*4, slice.Size()*4)
	segmentTree := &ArraySegmentTree{data: data, tree: tree, aggregator: aggregator}
	// 初始化线段树
	segmentTree.init(0, 0, slice.Size()-1)
	return segmentTree
}

/*
	创建一个动态线段树
*/
func CreateSegmentTree(slice array.Array, aggregator func(leftChildValue interface{}, rightChildValue interface{}) interface{}) *SegmentTree {
	data := make([]interface{}, slice.Size(), slice.Size())
	// copy原来的数组
	i := 0
	slice.Iterator(func(iterator common.Iterator) bool {
		v, _ := iterator.Get()
		data[i] = v
		i++
		return false
	})
	segmentTree := &SegmentTree{data: data, aggregator: aggregator}
	// 初始化线段树
	segmentTree.root = segmentTree.init(0, slice.Size()-1)
	return segmentTree
}

/*
	创建字典树
*/
func CreateTrie() *Trie {
	trie := &Trie{root: &trieNode{}}
	return trie
}

/*
	创建avl树
*/
func CreateAVLTree(comparator func(thisValue interface{}, compareValue interface{}) int) *AVLTree {
	return &AVLTree{comparator: comparator}
}

/*
	创建redBlack树
*/
func CreateRedBlackTree(comparator func(thisValue interface{}, compareValue interface{}) int) *RedBlackTree {
	return &RedBlackTree{comparator: comparator}
}

/*
	创建伸展树
*/
func CreateSplayTree(comparator func(thisValue interface{}, compareValue interface{}) int) *SplayTree {
	return &SplayTree{comparator: comparator}
}
