package tree

import (
	"fmt"
	"gitee.com/kklt1996/data-structure/array"
	"gitee.com/kklt1996/data-structure/common"
	"math/rand"
	"testing"
)

type compareAbleInt struct {
	Value int
}

func (c compareAbleInt) CompareTo(compareAble common.CompareAble) int {
	if c == compareAble {
		return 0
	} else if c.Value > compareAble.(compareAbleInt).Value {
		return 1
	} else {
		return -1
	}
}

func (c compareAbleInt) String() string {
	return fmt.Sprint(c.Value)
}

func TestBsTree_Add(t *testing.T) {
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	if tree.GetSize() != 3 {
		t.Errorf("excepted  size is 3 but is %v", tree.GetSize())
	}
}

func TestBsTree_Contains(t *testing.T) {
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	t.Log(tree.Contains(compareAbleInt{0}))
	t.Log(tree.Contains(compareAbleInt{1}))
	t.Log(tree.Contains(compareAbleInt{2}))
	t.Log(tree.Contains(compareAbleInt{3}))
}

func TestBsTree_IsEmpty(t *testing.T) {
	tree := &BsTree{}
	t.Log(tree.IsEmpty())
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	t.Log(tree.IsEmpty())
}

func TestBsTree_PreOrder(t *testing.T) {
	//			1
	//		   / \
	//		  0   2
	// 				\
	//    			 5
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	tree.Add(compareAbleInt{5}, nil)

	tree.PreOrder(func(key interface{}, value interface{}) bool {
		t.Log(key)
		if key.(compareAbleInt).Value == 2 {
			return true
		}
		return false
	})
}

func TestBsTree_PreOrderNR(t *testing.T) {
	//			1
	//		   / \
	//		  0   2
	// 				\
	//    			 5
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	tree.Add(compareAbleInt{5}, nil)

	tree.PreOrderNR(func(key interface{}, value interface{}) bool {
		t.Log(key)
		return false
	})
}

func TestBsTree_InOrder(t *testing.T) {
	//			1
	//		   / \
	//		  0   2
	// 				\
	//    			 5
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	tree.Add(compareAbleInt{5}, nil)

	tree.InOrder(func(key interface{}, value interface{}) bool {
		t.Log(key)
		return false
	})
}

func TestBsTree_PostOrder(t *testing.T) {
	//			1
	//		   / \
	//		  0   2
	// 				\
	//    			 5
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	tree.Add(compareAbleInt{5}, nil)
	tree.PostOrder(func(key interface{}, value interface{}) bool {
		t.Log(key)
		return false
	})
}

func TestBsTree_String(t *testing.T) {
	//			1
	//		   / \
	//		  0   2
	// 				\
	//    			 5
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	tree.Add(compareAbleInt{5}, nil)
	fmt.Print(tree.String())
}

func TestBsTree_LevelOrder(t *testing.T) {
	//			1
	//		   / \
	//		  0   2
	// 				\
	//    			 5
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	tree.Add(compareAbleInt{5}, nil)
	tree.LevelOrder(func(key interface{}, value interface{}) bool {
		t.Log(key)
		return false
	})
}

func TestBsTree_Minimum(t *testing.T) {
	//			1
	//		   / \
	//		  0   2
	// 				\
	//    			 5
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	tree.Add(compareAbleInt{5}, nil)
	minimumKey, _, _ := tree.Minimum()
	t.Log(minimumKey)
}

func TestBsTree_Maximum(t *testing.T) {
	//			1
	//		   / \
	//		  0   2
	// 				\
	//    			 5
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	tree.Add(compareAbleInt{5}, nil)
	maximum, _, _ := tree.Maximum()
	t.Log(maximum)
}

func TestBsTree_RemoveMinimum(t *testing.T) {
	//			1
	//		   / \
	//		  0   2
	// 				\
	//    			 5
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	tree.Add(compareAbleInt{5}, nil)
	minimum, _, _ := tree.RemoveMinimum()
	t.Log(minimum)
	t.Log(tree)
}

func TestBsTree_RemoveMaximum(t *testing.T) {
	//			1
	//		   / \
	//		  0   2
	// 				\
	//    			 5
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	tree.Add(compareAbleInt{5}, nil)
	maximum, _, _ := tree.RemoveMaximum()
	t.Logf("remove maximum is %v", maximum)
	t.Log(tree)
}

func TestBsTree_RemoveElement(t *testing.T) {
	//			1
	//		   / \
	//		  0   2
	// 				\
	//    			 5
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	tree.Add(compareAbleInt{5}, nil)
	_, _ = tree.RemoveElement(compareAbleInt{2})
	t.Log(tree)
}

func TestBsTree_MaxDepth(t *testing.T) {
	//			1
	//		   / \
	//		  0   2
	// 				\
	//    			 5
	tree := &BsTree{}
	tree.Add(compareAbleInt{1}, nil)
	tree.Add(compareAbleInt{2}, nil)
	tree.Add(compareAbleInt{0}, nil)
	tree.Add(compareAbleInt{5}, nil)
	tree.Add(compareAbleInt{6}, nil)
	tree.Add(compareAbleInt{-10}, nil)
	tree.Add(compareAbleInt{-5}, nil)
	tree.Add(compareAbleInt{-6}, nil)
	t.Log(tree.MaxDepth())
}

func TestCreateSegmentTree(t *testing.T) {
	arrayDefault := array.CreateSliceArrayDefault()
	arrayDefault.AddLast(1)
	arrayDefault.AddLast(2)
	arrayDefault.AddLast(3)
	arrayDefault.AddLast(4)
	arrayDefault.AddLast(5)
	tree := CreateArraySegmentTree(arrayDefault, func(leftChildValue interface{}, rightChildValue interface{}) interface{} {
		if rightChildValue != nil && leftChildValue != nil {
			return rightChildValue.(int) + leftChildValue.(int)
		} else if rightChildValue != nil {
			return rightChildValue
		} else {
			return leftChildValue
		}
	})
	fmt.Print(tree)
}

func TestSegmentTree_Query(t *testing.T) {
	arrayDefault := array.CreateSliceArrayDefault()
	arrayDefault.AddLast(1)
	arrayDefault.AddLast(2)
	arrayDefault.AddLast(3)
	arrayDefault.AddLast(4)
	arrayDefault.AddLast(5)
	tree := CreateArraySegmentTree(arrayDefault, func(leftChildValue interface{}, rightChildValue interface{}) interface{} {
		if rightChildValue != nil && leftChildValue != nil {
			return rightChildValue.(int) + leftChildValue.(int)
		} else if rightChildValue != nil {
			return rightChildValue
		} else {
			return leftChildValue
		}
	})
	fmt.Println(tree)
	fmt.Println(tree.Query(0, 4))
	fmt.Println(tree.Query(0, 1))
	fmt.Println(tree.Query(1, 2))
	fmt.Println(tree.Query(1, 4))
}

func TestCreateTrie(t *testing.T) {
	trie := CreateTrie()
	trie.Add("李守余是好人")
}

func TestTrie_Add(t *testing.T) {
	trie := CreateTrie()
	trie.Add("李守余是好人")
}

func TestTrie_Contains(t *testing.T) {
	trie := CreateTrie()
	trie.Add("李守余是好人")
	contains := trie.Contains("李守余是好人")
	fmt.Println(contains)
	fmt.Println(trie.GetSize())
}

func TestTrie_GetSize(t *testing.T) {
	trie := CreateTrie()
	trie.Add("李守余是好人")
	trie.Add("李守余是好人1")
	contains := trie.Contains("李守余是好人")
	fmt.Println(contains)
	fmt.Println(trie.GetSize())
}

func TestTrie_IsPrefix(t *testing.T) {
	trie := CreateTrie()
	trie.Add("李守余是好人")
	trie.Add("李守余是好人1")
	isPrefix := trie.IsPrefix("李守余")
	fmt.Println(isPrefix)
}

func TestTrie_Match(t *testing.T) {
	trie := CreateTrie()
	trie.Add("李守余是好人")
	trie.Add("李守余是好人1")
	match := trie.Match(".守余是好人")
	fmt.Println(match)
}

func TestTrie_PrefixWords(t *testing.T) {
	trie := CreateTrie()
	trie.Add("1李守余是好人")
	trie.Add("李守余是好人1")
	match := trie.PrefixWords("李守余是好人")
	fmt.Println(match)
}

func TestAVLTree_Add(t *testing.T) {
	avlTree := CreateAVLTree(func(thisValue interface{}, compareValue interface{}) int {
		i := thisValue.(int)
		j := compareValue.(int)
		if i == j {
			return 0
		} else if i > j {
			return 1
		} else {
			return -1
		}
	})
	for i := 0; i < 10000; i++ {
		avlTree.Add(rand.Int(), nil)
		if !avlTree.IsBSTTree() {
			t.Error("avlTree is not bstTree")
		}
		if !avlTree.IsBalanceTree() {
			t.Error("avlTree is not balance")
		}
	}
}

func TestAVLTree_RemoveElement(t *testing.T) {
	avlTree := CreateAVLTree(func(thisValue interface{}, compareValue interface{}) int {
		i := thisValue.(int)
		j := compareValue.(int)
		if i == j {
			return 0
		} else if i > j {
			return 1
		} else {
			return -1
		}
	})
	for i := 0; i < 10000; i++ {
		avlTree.Add(i, nil)
		if !avlTree.IsBSTTree() {
			t.Error("avlTree is not bstTree")
		}
		if !avlTree.IsBalanceTree() {
			t.Error("avlTree is not balance")
		}
	}
	for i := 0; i < 10000; i++ {
		avlTree.RemoveElement(i)
		if !avlTree.IsBSTTree() {
			t.Error("avlTree is not bstTree")
		}
		if !avlTree.IsBalanceTree() {
			t.Error("avlTree is not balance")
		}
	}
}

func TestCreateSplayTree(t *testing.T) {
	tree := CreateSplayTree(nil)
	if tree == nil {
		t.Error("create splay tree error")
	}
}

func TestSplayTree_Add(t *testing.T) {
	tree := CreateSplayTree(nil)
	if tree == nil {
		t.Error("create splay tree error")
	}
	for i := 1; i < 16; i++ {
		tree.Add(i, i)
	}
	if tree.root.value != 15 {
		t.Error("splay tree expected root is 15 but is", tree.root.value)
	}
}

func TestSplayTree_Contains(t *testing.T) {
	tree := CreateSplayTree(nil)
	if tree == nil {
		t.Error("create splay tree error")
	}
	for i := 1; i < 16; i++ {
		tree.Add(i, i)
	}

	if tree.root.value != 15 {
		t.Error("splay tree expected root is 15 but is", tree.root.value)
	}

	for i := 1; i < 16; i++ {
		tree.Contains(i)
		if tree.root.value != i {
			t.Error("splay tree expected root is ", i, " but is", tree.root.value)
		}
	}
}

func TestSplayTree_Get(t *testing.T) {
	tree := CreateSplayTree(nil)
	if tree == nil {
		t.Error("create splay tree error")
	}
	for i := 1; i < 16; i++ {
		tree.Add(i, i)
	}

	if tree.root.value != 15 {
		t.Error("splay tree expected root is 15 but is", tree.root.value)
	}

	for i := 1; i < 16; i++ {
		get := tree.Get(i)
		if get != i {
			t.Error("splay tree expected get value is ", i, " but is", get)
		}
		if tree.root.value != i {
			t.Error("splay tree expected root is ", i, " but is", tree.root.value)
		}
	}
}

func TestSplayTree_RemoveMinElement(t *testing.T) {
	tree := CreateSplayTree(nil)
	if tree == nil {
		t.Error("create splay tree error")
	}
	for i := 1; i < 16; i++ {
		tree.Add(i, i)
	}

	if tree.root.value != 15 {
		t.Error("splay tree expected root is 15 but is", tree.root.value)
	}

	for i := 1; i < 16; i++ {
		get, _ := tree.RemoveElement(i)
		t.Log("删除最小元素后最大深度是:", tree.MaxDepth())
		if get != i {
			t.Error("splay tree expected get value is ", i, " but is", get)
		}
	}
}

func TestSplayTree_RemoveMaxElement(t *testing.T) {
	tree := CreateSplayTree(nil)
	if tree == nil {
		t.Error("create splay tree error")
	}
	for i := 1; i < 16; i++ {
		tree.Add(i, i)
	}

	if tree.root.value != 15 {
		t.Error("splay tree expected root is 15 but is", tree.root.value)
	}

	for i := 15; i > 0; i-- {
		get, _ := tree.RemoveElement(i)
		t.Log("删除最大元素后最大深度是:", tree.MaxDepth())
		if get != i {
			t.Error("splay tree expected get value is ", i, " but is", get)
		}
	}
}
