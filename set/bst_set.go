package set

import (
	"gitee.com/kklt1996/data-structure/tree"
)

type BstSet struct {
	/*
		存储集合的容器
	*/
	container *tree.BsTree
}

func (b BstSet) Add(value interface{}) {
	b.container.Add(value, nil)
}

func (b BstSet) Remove(value interface{}) error {
	_, err := b.container.RemoveElement(value)
	return err
}

func (b BstSet) Contains(value interface{}) bool {
	return b.container.Contains(value)
}

func (b BstSet) GetSize() int {
	return b.container.GetSize()
}

func (b BstSet) IsEmpty() bool {
	return b.container.IsEmpty()
}

func (b BstSet) String() string {
	return b.container.String()
}
