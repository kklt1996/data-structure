package set

import (
	"fmt"
	"gitee.com/kklt1996/data-structure/tree"
)

/*
	创建二分搜索树实现的集合
*/
func CreateBstSet(comparator func(thisValue interface{}, compareValue interface{}) int) BstSet {
	bstTree := tree.CreateBstTree(comparator)
	return BstSet{container: bstTree}
}

/*
	集合的解耦定义
*/
type Set interface {
	fmt.Stringer

	/*
		添加元素
		添加重复的元素会被覆盖
	*/
	Add(value interface{})

	/*
		删除元素
	*/
	Remove(value interface{}) error

	/*
		查询结合中是否包含某一元素
	*/
	Contains(value interface{}) bool

	/*
		查看集合元素个数
	*/
	GetSize() int

	/*
		是否为空
	*/
	IsEmpty() bool
}
