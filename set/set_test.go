package set

import (
	"fmt"
	"gitee.com/kklt1996/data-structure/common"
	"testing"
)

type compareAbleInt struct {
	Value int
}

func (c compareAbleInt) CompareTo(compareAble common.CompareAble) int {
	if c == compareAble {
		return 0
	} else if c.Value > compareAble.(compareAbleInt).Value {
		return 1
	} else {
		return -1
	}
}

func (c compareAbleInt) String() string {
	return fmt.Sprint(c.Value)
}

func TestBstSet_Add(t *testing.T) {
	set := CreateBstSet(nil)
	set.Add(compareAbleInt{Value: 3})
	set.Add(compareAbleInt{Value: 1})
	set.Add(compareAbleInt{Value: 2})
	fmt.Println(set.String())
}

func TestBstSet_Remove(t *testing.T) {
	set := CreateBstSet(nil)
	set.Add(compareAbleInt{Value: 3})
	set.Add(compareAbleInt{Value: 1})
	set.Add(compareAbleInt{Value: 2})
	set.Remove(compareAbleInt{Value: 3})
	fmt.Println(set.String())
}

func TestBstSet_Contains(t *testing.T) {
	set := CreateBstSet(nil)
	set.Add(compareAbleInt{Value: 3})
	set.Add(compareAbleInt{Value: 1})
	set.Add(compareAbleInt{Value: 2})
	set.Contains(compareAbleInt{Value: 3})
	fmt.Println(set.Contains(compareAbleInt{Value: 3}))
}

func TestBstSet_GetSize(t *testing.T) {
	set := CreateBstSet(nil)
	set.Add(compareAbleInt{Value: 3})
	set.Add(compareAbleInt{Value: 1})
	set.Remove(compareAbleInt{Value: 1})
	set.Add(compareAbleInt{Value: 2})
	fmt.Println(set.GetSize())
	fmt.Println(set.String())
}

func TestBstSet_IsEmpty(t *testing.T) {
	set := CreateBstSet(nil)
	set.Add(compareAbleInt{Value: 3})
	set.Add(compareAbleInt{Value: 1})
	set.Add(compareAbleInt{Value: 2})
	fmt.Println(set.IsEmpty())
	fmt.Println(set.String())
	set.Remove(compareAbleInt{Value: 3})
	set.Remove(compareAbleInt{Value: 1})
	set.Remove(compareAbleInt{Value: 2})
	fmt.Println(set.IsEmpty())
	fmt.Println(set.String())
}
