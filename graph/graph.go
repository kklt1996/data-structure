package graph

/*
	创建稠密图
	n 顶点的数量
    directed 是否是有向图
*/
func CreateDenseGraph(n int, directed bool) *DenseGraph {
	denseGraph := &DenseGraph{}
	denseGraph.n = n
	denseGraph.directed = directed

	// 边的数量
	denseGraph.m = 0

	// 初始化矩阵
	g := make([][]bool, n, n)
	for i := 0; i < n; i++ {
		g[i] = make([]bool, n, n)
	}
	denseGraph.g = g
	return denseGraph
}

func CreateSparseGraph(n int, directed bool) *SparseGraph {
	sparseGraph := &SparseGraph{}

	sparseGraph.n = n
	sparseGraph.directed = directed

	sparseGraph.m = 0

	arr := make([][]int, n, n)
	for i := 0; i < n; i++ {
		arr[i] = make([]int, 0)
	}
	sparseGraph.g = arr
	return sparseGraph
}
