package graph

import (
	"fmt"
	"math/rand"
	"testing"
)

func TestDenseGraph_VertexIterator(t *testing.T) {
	n := 10
	graph := CreateDenseGraph(n, false)
	r := rand.New(rand.NewSource(100))
	for i := 0; i < 100; i++ {
		graph.AddEdge(r.Int()%n, r.Int()%n)
	}
	for i := 0; i < n; i++ {
		vertexIterator := graph.VertexIterator(i)
		fmt.Printf("%d :", i)
		for w := vertexIterator.Begin(); !vertexIterator.End(); w = vertexIterator.Next() {
			fmt.Printf(" %d", w)

		}
		fmt.Println()
	}
}

func TestSparseGraph_VertexIterator(t *testing.T) {
	n := 10
	graph := CreateSparseGraph(n, false)
	r := rand.New(rand.NewSource(10))
	for i := 0; i < 10; i++ {
		graph.AddEdge(r.Int()%n, r.Int()%n)
	}
	for i := 0; i < n; i++ {
		vertexIterator := graph.VertexIterator(i)
		fmt.Printf("%d :", i)
		for w := vertexIterator.Begin(); !vertexIterator.End(); w = vertexIterator.Next() {
			fmt.Printf(" %d", w)

		}
		fmt.Println()
	}
}
