package queue

import "gitee.com/kklt1996/data-structure/array"

/*
	数组实现的队列,由于ArrayQueue是基于slice实现的,并不是真正的动态数组．所以其算法渐进时间复杂度并不高．
	在出队和入队操作量不是很大的时候性能甚至高于LoopQueue
	在出队和入队操作量很大的时候,LoopQueue性能更高一些但相差不多，并不是量级的差异
*/
type ArrayQueue struct {
	array *array.SliceArray
}

/*
	~=O(1)
	队尾添加元素
*/
func (a ArrayQueue) Enqueue(item interface{}) {
	a.array.AddLast(item)
}

/*
	~=O(1)
	队首元素出队
*/
func (a ArrayQueue) Dequeue() (interface{}, error) {
	return a.array.RemoveFirst()
}

/*
	查看队首元素
*/
func (a ArrayQueue) GetFront() (interface{}, error) {
	return a.array.GetFirst()
}

/*
	获取队列大小
*/
func (a ArrayQueue) GetSize() int {
	return a.array.Size()
}

/*
	获取队列是否为空
*/
func (a ArrayQueue) IsEmpty() bool {
	return a.array.IsEmpty()
}

func (a ArrayQueue) String() string {
	return a.array.String()
}
