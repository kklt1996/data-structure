package queue

import (
	"testing"
	"time"
)

type TestLoopEnqueueStruct struct {
	before       Queue
	addItem      []int
	exceptedSize int
}

func TestArrayQueue_Enqueue(t *testing.T) {
	var testEnqueue []TestLoopEnqueueStruct

	queueDefault0 := CreateArrayQueueDefault()
	queueDefault0.Enqueue(1)
	testEnqueue = append(testEnqueue, TestLoopEnqueueStruct{queueDefault0, []int{1}, 2})

	queueDefault1 := CreateArrayQueueDefault()
	queueDefault1.Enqueue(1)
	queueDefault1.Enqueue(2)
	queueDefault1.Enqueue(3)
	queueDefault1.Enqueue(4)
	queueDefault1.Enqueue(5)
	queueDefault1.Enqueue(6)
	queueDefault1.Enqueue(7)
	queueDefault1.Enqueue(8)
	queueDefault1.Enqueue(9)
	queueDefault1.Enqueue(10)
	queueDefault1.Enqueue(11)
	queueDefault1.Enqueue(12)
	queueDefault1.Enqueue(13)
	queueDefault1.Enqueue(14)
	queueDefault1.Enqueue(15)
	testEnqueue = append(testEnqueue, TestLoopEnqueueStruct{queueDefault1, []int{16, 17}, 17})
	for _, v := range testEnqueue {
		t.Logf("add before %s, add item: %v", v.before, v.addItem)
		for _, value := range v.addItem {
			v.before.Enqueue(value)
		}
		t.Logf("add after %s", v.before)
		if v.before.GetSize() != v.exceptedSize {
			t.Errorf("exceptedSize is %d, resultSize is %d", v.exceptedSize, v.before.GetSize())
		}
	}
}

func TestLoopQueue_Enqueue(t *testing.T) {
	var testEnqueue []TestLoopEnqueueStruct

	queueDefault0 := CreateLoopQueueDefault()
	queueDefault0.Enqueue(1)
	testEnqueue = append(testEnqueue, TestLoopEnqueueStruct{queueDefault0, []int{1}, 2})

	queueDefault1 := CreateLoopQueueDefault()
	queueDefault1.Enqueue(1)
	queueDefault1.Enqueue(2)
	queueDefault1.Enqueue(3)
	queueDefault1.Enqueue(4)
	queueDefault1.Enqueue(5)
	queueDefault1.Enqueue(6)
	queueDefault1.Enqueue(7)
	queueDefault1.Enqueue(8)
	queueDefault1.Enqueue(9)
	queueDefault1.Enqueue(10)
	queueDefault1.Enqueue(11)
	queueDefault1.Enqueue(12)
	queueDefault1.Enqueue(13)
	queueDefault1.Enqueue(14)
	queueDefault1.Enqueue(15)
	testEnqueue = append(testEnqueue, TestLoopEnqueueStruct{queueDefault1, []int{16, 17}, 17})
	for _, v := range testEnqueue {
		t.Logf("add before %s, add item: %v", v.before, v.addItem)
		for _, value := range v.addItem {
			v.before.Enqueue(value)
		}
		t.Logf("add after %s", v.before)
		if v.before.GetSize() != v.exceptedSize {
			t.Errorf("exceptedSize is %d, resultSize is %d", v.exceptedSize, v.before.GetSize())
		}
	}
}

func BenchmarkArrayQueue_Dequeue(b *testing.B) {
	queueDefault1 := CreateArrayQueueDefault()
	queueDefault1.Enqueue(1)
	queueDefault1.Enqueue(2)
	queueDefault1.Enqueue(3)
	queueDefault1.Enqueue(4)
	queueDefault1.Enqueue(5)
	queueDefault1.Enqueue(6)
	queueDefault1.Enqueue(7)
	queueDefault1.Enqueue(8)
	queueDefault1.Enqueue(9)
	queueDefault1.Enqueue(10)
	queueDefault1.Enqueue(11)
	queueDefault1.Enqueue(12)
	queueDefault1.Enqueue(13)
	queueDefault1.Enqueue(14)
	for i := 0; i < b.N; i++ {
		start := time.Now().Nanosecond()
		for j := 0; j < 100000; j++ {
			queueDefault1.Enqueue(15)
		}
		for j := 0; j < 100000; j++ {
			queueDefault1.Dequeue()
		}
		b.Log(time.Now().Nanosecond() - start)
	}
}

func BenchmarkLoopQueue_Dequeue(b *testing.B) {
	queueDefault1 := CreateLoopQueueDefault()
	queueDefault1.Enqueue(1)
	queueDefault1.Enqueue(2)
	queueDefault1.Enqueue(3)
	queueDefault1.Enqueue(4)
	queueDefault1.Enqueue(5)
	queueDefault1.Enqueue(6)
	queueDefault1.Enqueue(7)
	queueDefault1.Enqueue(8)
	queueDefault1.Enqueue(9)
	queueDefault1.Enqueue(10)
	queueDefault1.Enqueue(11)
	queueDefault1.Enqueue(12)
	queueDefault1.Enqueue(13)
	queueDefault1.Enqueue(14)
	for i := 0; i < b.N; i++ {
		start := time.Now().Nanosecond()
		for j := 0; j < 100000; j++ {
			queueDefault1.Enqueue(15)
		}
		for j := 0; j < 100000; j++ {
			queueDefault1.Dequeue()
		}
		b.Log(time.Now().Nanosecond() - start)
	}
}

func BenchmarkLinkedListQueue_Dequeue(b *testing.B) {
	queueDefault1 := CreateLinkedListQueue()
	queueDefault1.Enqueue(1)
	queueDefault1.Enqueue(2)
	queueDefault1.Enqueue(3)
	queueDefault1.Enqueue(4)
	queueDefault1.Enqueue(5)
	queueDefault1.Enqueue(6)
	queueDefault1.Enqueue(7)
	queueDefault1.Enqueue(8)
	queueDefault1.Enqueue(9)
	queueDefault1.Enqueue(10)
	queueDefault1.Enqueue(11)
	queueDefault1.Enqueue(12)
	queueDefault1.Enqueue(13)
	queueDefault1.Enqueue(14)
	for i := 0; i < b.N; i++ {
		for j := 0; j < 100000; j++ {
			queueDefault1.Enqueue(15)
		}
		start := time.Now().Nanosecond()
		for j := 0; j < 100000; j++ {
			queueDefault1.Dequeue()
		}
		b.Log(time.Now().Nanosecond() - start)
	}
}
