package queue

import (
	"gitee.com/kklt1996/data-structure/heap"
)

type PriorityQueue struct {
	data heap.MaxHeap
}

func (queue PriorityQueue) Enqueue(item interface{}) {
	queue.data.Add(item)
}

func (queue PriorityQueue) Dequeue() (interface{}, error) {
	return queue.data.ExtractTop()
}

func (queue PriorityQueue) GetFront() (interface{}, error) {
	return queue.data.FindTop()
}

func (queue PriorityQueue) GetSize() int {
	return queue.data.GetSize()
}

func (queue PriorityQueue) IsEmpty() bool {
	return queue.data.IsEmpty()
}
