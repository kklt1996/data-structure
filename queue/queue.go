package queue

import (
	"fmt"
	"gitee.com/kklt1996/data-structure/array"
	"gitee.com/kklt1996/data-structure/heap"
)

/*
	创建一个默认容量的队列
*/
func CreateArrayQueueDefault() ArrayQueue {
	arr := array.CreateSliceArrayDefault()
	arrayQueue := ArrayQueue{arr}
	return arrayQueue
}

/*
	创建一个指定容量的队列
*/
func CreateArrayQueue(capacity int) ArrayQueue {
	arr := array.CreateSliceArray(0, capacity)
	arrayQueue := ArrayQueue{arr}
	return arrayQueue
}

/*
	创建一个默认容量的循环队列,默认容量是16
*/
func CreateLoopQueueDefault() *LoopQueue {
	return CreateLoopQueue(16)
}

/*
	创建指定容量的循环队列
*/
func CreateLoopQueue(capacity int) *LoopQueue {
	// 因为循环队列的容量等于数组的容量-1,因此需要在用户期望的容量上加1
	arr := make([]interface{}, capacity+1, capacity+1)
	queue := LoopQueue{data: arr, front: 0, tail: 0, size: 0}
	return &queue
}

/*
	创建基于链表的队列
*/
func CreateLinkedListQueue() *LinkedListQueue {
	return &LinkedListQueue{head: nil, tail: nil, size: 0}
}

/*
	创建优先队列
*/
func CreatePriorityQueue(comparator func(thisValue interface{}, compareValue interface{}) int) PriorityQueue {
	heapDefault := heap.CreateMaxHeapDefault(comparator)
	return PriorityQueue{data: heapDefault}
}

/*
	队列的实现
*/

type Queue interface {
	fmt.Stringer

	/*
		队尾添加元素
	*/
	Enqueue(item interface{})
	/*
		队首元素出队
	*/
	Dequeue() (interface{}, error)
	/*
		查看队首元素
	*/
	GetFront() (interface{}, error)
	/*
		获取队列大小
	*/
	GetSize() int
	/*
		获取队列是否为空
	*/
	IsEmpty() bool
}
