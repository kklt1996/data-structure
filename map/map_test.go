package mapp

import (
	"fmt"
	"gitee.com/kklt1996/data-structure/common"
	"testing"
)

type compareAbleInt struct {
	Value int
}

func (c compareAbleInt) CompareTo(compareAble common.CompareAble) int {
	if c == compareAble {
		return 0
	} else if c.Value > compareAble.(compareAbleInt).Value {
		return 1
	} else {
		return -1
	}
}

func (c compareAbleInt) String() string {
	return fmt.Sprint(c.Value)
}

func TestBstMap_Put(t *testing.T) {
	bstMap := BstMap{}
	fmt.Println(bstMap.IsEmpty())
	fmt.Println(bstMap.GetSize())
	bstMap.Put(compareAbleInt{1}, 1)
	fmt.Println(bstMap.Contains(compareAbleInt{1}))
	fmt.Println(bstMap.IsEmpty())
	fmt.Println(bstMap.GetSize())
}

func TestBstMap_Remove(t *testing.T) {
	bstMap := BstMap{}
	fmt.Println(bstMap.IsEmpty())
	fmt.Println(bstMap.GetSize())
	bstMap.Put(compareAbleInt{1}, 2)
	bstMap.Put(compareAbleInt{2}, 3)
	fmt.Println(bstMap.Contains(compareAbleInt{1}))
	fmt.Println(bstMap.IsEmpty())
	fmt.Println(bstMap.GetSize())
	fmt.Println(bstMap.Remove(compareAbleInt{1}))
	fmt.Println(bstMap.IsEmpty())
	fmt.Println(bstMap.GetSize())
	fmt.Println(bstMap.Remove(compareAbleInt{2}))

}
