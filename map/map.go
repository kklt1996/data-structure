package mapp

/*
	创建基于二分搜索树的map
*/
func CreateBstMap(comparator func(thisValue interface{}, compareValue interface{}) int) *BstMap {
	return &BstMap{comparator: comparator}
}

type Map interface {

	/*
		添加修改元素
	*/
	Put(key interface{}, value interface{})

	/*
		删除元素
	*/
	Remove(key interface{}) interface{}

	/*
		获取元素
	*/
	Get(key interface{}) interface{}

	/*
		查询是否包含元素
	*/
	Contains(key interface{}) bool

	/*
		获取大小
	*/
	GetSize() int

	/*
		是否为空
	*/
	IsEmpty() bool
}
