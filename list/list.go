package list

import (
	"fmt"
	"gitee.com/kklt1996/data-structure/common"
	"math/rand"
)

/*
	创建单向链表
*/
func CreateLinkedList() *LinkedList {
	return &LinkedList{size: 0, dummyHead: &node{nil, nil}}
}

/*
	创建双向循环链表
*/
func CreateLoopList() *LoopList {
	return &LoopList{size: 0, dummyNode: &twoWayNode{prev: nil, value: nil, next: nil}}
}

/*
	创建跳跃表
*/
func CreateSkipList() *SkipList {
	skipList := &SkipList{}
	// 初始化跳跃表
	skipList.random = rand.New(rand.NewSource(1024))
	skipList.headSkipNodes = CreateLinkedList()
	skipList.tailSkipNodes = CreateLinkedList()
	skipList.curLevel = skipList.getRandomLevel()
	// 属性初始化完毕，初始化所有节点
	var lowHead *skipListNode
	var lowTail *skipListNode

	for i := 0; i <= skipList.curLevel; i++ {
		// 更新上一层节点
		lowHead, lowTail = skipList.addHighHeadTailNode(lowHead, lowTail, i == 0)
	}
	return skipList
}

/*
	列表接口
*/
type List interface {
	fmt.Stringer

	common.SortAble

	GetSize() int

	IsEmpty() bool

	Add(index int, value ...interface{}) error

	AddFirst(value ...interface{})

	AddLast(value ...interface{})

	Get(index int) (interface{}, error)

	GetFirst() (interface{}, error)

	GetLast() (interface{}, error)

	Set(index int, value interface{}) error

	Remove(index int) (interface{}, error)

	RemoveFirst() (interface{}, error)

	RemoveLast() (interface{}, error)

	/*
		删除第一个为value的元素
	*/
	RemoveElement(value interface{})

	/*
		用于按条件删除元素
	*/
	RemoveIfMatch(matchFunc func(value interface{}) bool) int

	/*
		删除所有值为value的元素
	*/
	RemoveAllElement(value interface{}) int

	Contains(value interface{}) bool

	/*
		循环中按条件查找或按条件查找设置节点值的属性(存储的是指针类型修改属性才生效)
	*/
	Foreach(foreachFunc func(index int, value interface{}) bool)
}
