package list

import (
	"fmt"
	"gitee.com/kklt1996/data-structure/common"
	"testing"
)

func Test_bit_operator(t *testing.T) {
	t.Log(1 >> 1)
	t.Log(2 >> 1)
}

func TestCreateLoopList(t *testing.T) {
	list := CreateLoopList()
	list.AddFirst(1, 2)
	list.AddLast(3)
	list.Add(2, 4)
	list.Remove(3)
	list.Remove(0)
	list.Iterator(func(iterator common.Iterator) bool {
		get, _ := iterator.Get()
		if get.(int) == 4 {
			iterator.Remove()
		}
		return false
	})
	t.Log(fmt.Print(list))
}

func TestCreateLinkedList(t *testing.T) {
	list := CreateLinkedList()
	list.AddFirst(1, 2)
	list.AddLast(3)
	list.Add(2, 4)
	list.Add(4)
	list.Add(4)
	list.Add(4)
	list.Remove(3)
	list.Remove(0)
	list.Iterator(func(iterator common.Iterator) bool {
		get, _ := iterator.Get()
		if get.(int) == 4 {
			iterator.Remove()
		}
		return false
	})
	t.Log(fmt.Print(list))
}

func TestCreateSkipList(t *testing.T) {
	list := CreateSkipList()
	t.Log("size 0 =", list.size == 0)
	list.Add(10, nil)
	t.Log("size 1 =", list.size == 1)
	_, ok := list.Find(10)
	t.Log("find score 10 = ", ok)

	list.Add(5, nil)
	t.Log("size 2 =", list.size == 2)

	_, ok = list.Find(5)
	t.Log("find score 5 = ", ok)

	list.Remove(10)
	t.Log("size 1 =", list.size == 1)

	_, ok = list.Find(10)
	t.Log("find score 10 = ", !ok)

	_, ok = list.Find(5)
	t.Log("find score 5 = ", ok)
}
