package common

/*
	for循环 int 类型迭代器
*/
type ForIntIterator interface {
	Begin() int

	Next() int

	End() bool
}

/*
	loopList迭代器,用于再遍历的过程中删除元素和修改元素
*/
type Iterator interface {
	Set(value interface{}) error

	Get() (interface{}, error)

	Remove()
}

type IteratorAble interface {
	/*
		增强迭代器,可以在迭代的过程中删除和修改元素
	*/
	Iterator(iteratorFunc func(iterator Iterator) bool)
}
