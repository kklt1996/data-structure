package common

/*
	索引异常
*/
type IndexError struct {
}

func (i IndexError) Error() string {
	return "非法的索引"
}

/*
	操作得不到预期结果异常
*/
type OperatorNotUseful struct {
}

func (i OperatorNotUseful) Error() string {
	return "此操作通常不能得到预期结果"
}
