package common

/*
	可以转化为切片
*/
type SliceAble interface {
	ToSlice() []interface{}
}

/*
	可以转换成切片,可以迭代的就是可以排序的数据结构
*/
type SortAble interface {
	IteratorAble

	SliceAble
}

/*
	表示结构体是可比较的
*/
type CompareAble interface {
	/*
			进行大小的比较
			-1  <= compareAble
			 0  == compareAble
		     1  >= compareAble
	*/
	CompareTo(compareAble CompareAble) int
}

/*
	常用比较函数定义
*/
type CompareFunc func(thisValue interface{}, compareValue interface{}) int

type Integer int

/*
	Integer 是可比较的
*/
func (integer Integer) CompareTo(compareAble CompareAble) int {
	value := compareAble.(Integer)
	if integer == value {
		return 0
	}
	if integer > value {
		return 1
	} else {
		return -1
	}
}
