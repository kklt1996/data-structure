package hash

import (
	"testing"
)

func TestCreateHashTable(t *testing.T) {
	hashTable := CreateHashTableDeFault(nil)
	t.Log(hashTable)
}

func TestHash(t *testing.T) {
	hashTable := CreateHashTableDeFault(nil)
	ints := make(map[int]int)
	for i := 0; i < 17*1000; i++ {
		ints[hashTable.hash(i)]++
	}
	for _, v := range ints {
		t.Log(v)
	}
	t.Log(int(capacityPrime[len(capacityPrime)-1] * 10))
}

func TestTable_Put(t *testing.T) {
	hashTable := CreateHashTableDeFault(func(thisKey interface{}, compareKey interface{}) int {
		i := thisKey.(int)
		j := compareKey.(int)
		if i == j {
			return 0
		} else if i > j {
			return 1
		} else {
			return -1
		}
	})
	hashTable.Put(1, 1)
	if hashTable.GetSize() != 1 {
		t.Error("size excepted 1 but ", hashTable.GetSize())
	}
}

func TestTable_Get(t *testing.T) {
	hashTable := CreateHashTableDeFault(func(thisKey interface{}, compareKey interface{}) int {
		i := thisKey.(int)
		j := compareKey.(int)
		if i == j {
			return 0
		} else if i > j {
			return 1
		} else {
			return -1
		}
	})
	hashTable.Put(1, 1)
	if hashTable.Get(1) != 1 {
		t.Error("get 1 excepted value is 1 but ", hashTable.Get(1))
	}
}

func TestTable_Remove(t *testing.T) {
	hashTable := CreateHashTableDeFault(func(thisKey interface{}, compareKey interface{}) int {
		i := thisKey.(int)
		j := compareKey.(int)
		if i == j {
			return 0
		} else if i > j {
			return 1
		} else {
			return -1
		}
	})
	hashTable.Put(1, 1)
	hashTable.Remove(1)
	if hashTable.Get(1) != nil {
		t.Error("get 1 excepted value is nil but ", hashTable.Get(1))
	}
}

func TestTable_resize(t *testing.T) {
	hashTable := CreateHashTableDeFault(func(thisKey interface{}, compareKey interface{}) int {
		i := thisKey.(int)
		j := compareKey.(int)
		if i == j {
			return 0
		} else if i > j {
			return 1
		} else {
			return -1
		}
	})
	for i := 0; i < 171; i++ {
		hashTable.Put(i, 1)
	}
	if hashTable.capacity != 31 {
		t.Error("hashtable excepted capa city is 31 but is ", hashTable.capacity)
	}
	if hashTable.size != 171 {
		t.Error("hashtable excepted size is 171 but is ", hashTable.size)
	}
	if hashTable.Get(170) != 1 {
		t.Error("get 171 excepted value is 1 but ", hashTable.Get(170))
	}
}
