package hash

import "gitee.com/kklt1996/data-structure/tree"

type Able interface {
	HashCode() string
}

/*
	创建hash表
*/
func CreateHashTableDeFault(comparator func(thisKey interface{},
	compareKey interface{}) int) *Table {

	return CreateHashTable(16, comparator)
}

/*
	comparator 可以为空,但是必须实现了common.CompareAble接口
*/
func CreateHashTable(capacity int,
	comparator func(thisKey interface{}, compareKey interface{}) int) *Table {
	lowerTol := 2
	upperTol := 10
	// 找到合适的容量
	capacityIndex := 0
	for i := 0; i < len(capacityPrime); i++ {
		if capacity <= capacityPrime[i]*upperTol {
			// 预期元素个数　小于　素数容量*每个哈希地址最大容纳的元素个数　(保证这么多元素不需要扩容)
			capacity = capacityPrime[i]
			capacityIndex = i
			break
		}
	}

	// 初始化哈希表
	data := make([]*tree.RedBlackTree, capacity, capacity)
	for i := 0; i < capacity; i++ {
		redBlackTree := tree.CreateRedBlackTree(comparator)
		data[i] = redBlackTree
	}
	table := Table{hashtable: data, capacity: capacity, size: 0, upperTol: upperTol, lowerTol: lowerTol,
		comparator: comparator, capacityIndex: capacityIndex}

	return &table
}
