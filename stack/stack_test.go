package stack

import (
	"testing"
)

var testGetSizeData = []struct {
	stack        Stack
	exceptedSize int
}{
	{CreateLinkedListStack(), 1},
}

func TestGetSize(t *testing.T) {
	for _, v := range testGetSizeData {
		v.stack.Push(1)
		getSize := v.stack.GetSize()
		if getSize != v.exceptedSize {
			t.Errorf("exceptedSize: %d, acctualSize: %d", v.exceptedSize, getSize)
		}
	}
}
