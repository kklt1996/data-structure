package stack

import "gitee.com/kklt1996/data-structure/array"

/*
	基于动态数组实现的stack
*/
type ArrayStack struct {
	list *array.SliceArray
}

/*
	栈是不是空的
*/
func (a ArrayStack) IsEmpty() bool {
	return a.list.IsEmpty()
}

/*
	~=O(1)
	向栈顶添加元素
*/
func (a ArrayStack) Push(element interface{}) {
	a.list.AddLast(element)
}

/*
	~=O(1)
	弹出并获取栈顶的元素
*/
func (a ArrayStack) Pop() (interface{}, error) {
	return a.list.RemoveLast()
}

/*
	查看栈顶的元素
*/
func (a ArrayStack) Peek() (interface{}, error) {
	return a.list.GetLast()
}

/*
	获取栈的大小
*/
func (a ArrayStack) GetSize() int {
	return a.list.Size()
}
