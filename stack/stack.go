package stack

import (
	"gitee.com/kklt1996/data-structure/array"
	"gitee.com/kklt1996/data-structure/list"
)

/*
	创建一个默认容量的栈
*/
func CreateArrayStackDefault() ArrayStack {
	arr := array.CreateSliceArrayDefault()
	arrayStack := ArrayStack{arr}
	return arrayStack
}

/*
	创建一个指定容量的栈
*/
func CreateArrayStack(capacity int) ArrayStack {
	arr := array.CreateSliceArray(0, capacity)
	arrayStack := ArrayStack{arr}
	return arrayStack
}

/*
	创建一个基于链表的栈
*/
func CreateLinkedListStack() LinkedListStack {
	return LinkedListStack{list.CreateLinkedList()}
}

/*
	栈的接口
*/
type Stack interface {
	/*
		获取栈的大小
	*/
	GetSize() int
	/*
		栈是不是空的
	*/
	IsEmpty() bool
	/*
		向栈顶添加元素
	*/
	Push(element interface{})
	/*
		弹出并获取栈顶的元素
	*/
	Pop() (interface{}, error)
	/*
		查看栈顶的元素
	*/
	Peek() (interface{}, error)
}
