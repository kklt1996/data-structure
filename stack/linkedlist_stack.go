package stack

import "gitee.com/kklt1996/data-structure/list"

type LinkedListStack struct {
	linkedList *list.LinkedList
}

/*
	栈是不是空的
*/
func (stack LinkedListStack) IsEmpty() bool {
	return stack.linkedList.IsEmpty()
}

/*
	O(1)
	向栈顶添加元素
*/
func (stack LinkedListStack) Push(element interface{}) {
	stack.linkedList.AddFirst(element)
}

/*
	O(1)
	弹出并获取栈顶的元素
*/
func (stack LinkedListStack) Pop() (interface{}, error) {
	return stack.linkedList.RemoveFirst()
}

/*
	查看栈顶的元素
*/
func (stack LinkedListStack) Peek() (interface{}, error) {
	return stack.linkedList.GetFirst()
}

/*
	获取栈的大小
*/
func (stack LinkedListStack) GetSize() int {
	return stack.linkedList.GetSize()
}
