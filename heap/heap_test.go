package heap

import (
	"fmt"
	"gitee.com/kklt1996/data-structure/common"
	"testing"
)

func TestCreateMaxHeap(t *testing.T) {
	heap := CreateMaxHeap(nil, 32)
	heap.Add(common.Integer(1))
	heap.Add(common.Integer(2))
}

func TestCreateMaxHeapDefault(t *testing.T) {
	heap := CreateMaxHeapDefault(func(thisValue interface{}, compareValue interface{}) int {
		if thisValue.(int) > compareValue.(int) {
			return 1
		} else {
			return -1
		}
	})
	heap.Add(1)
	heap.Add(2)
	heap.Add(3)
	heap.Add(4)
	heap.Add(5)
	heap.Add(6)
	heap.Add(9)
	heap.Add(8)
	fmt.Println(heap.ExtractTop())
	fmt.Println(heap.FindTop())
}
