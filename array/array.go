package array

import (
	"fmt"
	"gitee.com/kklt1996/data-structure/common"
)

/*
	创建默认大小的动态数组
*/
func CreateSliceArrayDefault() *SliceArray {
	return CreateSliceArray(0, 16)
}

/*
	创建动态数组
*/
func CreateSliceArray(size int, capacity int) *SliceArray {
	var slice SliceArray = make([]interface{}, size, capacity)
	return &slice
}

/*
	创建默认容量的自定义扩容策略动态数组
*/
func CreateDynamicArrayDefault() *DynamicArray {
	return CreateDynamicArray(16)
}

/*
	创建自定义容量的自定义扩容策略动态数组
*/
func CreateDynamicArray(capacity int) *DynamicArray {
	data := make([]interface{}, capacity, capacity)
	return &DynamicArray{data: data}
}

type Array interface {
	fmt.Stringer

	common.SortAble

	Size() int

	IsEmpty() bool

	Add(index int, item ...interface{})

	AddFirst(item ...interface{})

	AddLast(item ...interface{})

	Remove(index int) (interface{}, error)

	RemoveFirst() (interface{}, error)

	RemoveLast() (interface{}, error)

	RemoveElement(value interface{})

	RemoveIfMatch(matchFunc func(interface{}) bool) int

	RemoveAllElement(value interface{}) int

	Set(index int, value interface{})

	Get(index int) (interface{}, error)

	GetFirst() (interface{}, error)

	GetLast() (interface{}, error)

	Contains(value interface{}) bool

	FindFirst(value interface{}) int

	FindLast(value interface{}) int
}
