package array

import (
	"fmt"
	"gitee.com/kklt1996/data-structure/common"
	"gitee.com/kklt1996/data-structure/util"
	"math/rand"
	"testing"
)

func TestArray_Add(t *testing.T) {
	var testAddLastData = []struct {
		data         SliceArray
		addData      SliceArray
		exceptedData SliceArray
	}{
		{SliceArray{1, 2, 3, 4}, []interface{}{5}, SliceArray{1, 2, 3, 4, 5}},
		{SliceArray{}, SliceArray{1, 2, 3, 4, 5}, SliceArray{1, 2, 3, 4, 5}},
	}
	for _, v := range testAddLastData {
		data := v.data
		v.data.AddLast(v.addData...)
		if v.exceptedData.Size() != v.data.Size() {
			t.Errorf("%+v addLast %+v excepted %+v but %+v", data, v.addData, v.exceptedData, v.data)
		} else {
			for i := 0; i < v.exceptedData.Size()-1; i++ {
				if v.exceptedData[i] != v.data[i] {
					t.Errorf("%+v addLast %+v excepted %+v but %+v", data, v.addData, v.exceptedData, v.data)
				}
			}
		}
	}
}

func TestArray_RemoveAllElement(t *testing.T) {
	var testRemoveElement = []struct {
		data         SliceArray
		removeData   int
		exceptedData SliceArray
	}{
		{SliceArray{1, 2, 3, 4}, 2, SliceArray{1, 3, 4}},
		{SliceArray{1, 2, 2, 4}, 2, SliceArray{1, 4}},
	}
	for _, v := range testRemoveElement {
		data := v.data
		v.data.RemoveAllElement(v.removeData)
		if v.exceptedData.Size() != v.data.Size() {
			t.Errorf("%+v removeAll %+v excepted %+v but %+v", data, v.removeData, v.exceptedData, v.data)
		} else {
			for i := 0; i < v.exceptedData.Size()-1; i++ {
				if v.exceptedData[i] != v.data[i] {
					t.Errorf("%+v removeAll %+v excepted %+v but %+v", data, v.removeData, v.exceptedData, v.data)
				}
			}
		}
	}
}

func TestSliceArray_Remove(t *testing.T) {
	arr := CreateSliceArrayDefault()
	arr.AddLast(-1, 0)
	add := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33}
	for _, v := range add {
		arr.Add(1, v)
		t.Logf("add %v cap is %v", v, cap(*arr))
	}
	for i := 33; i > 0; i-- {
		arr.RemoveLast()
		t.Logf("%v cap is %v", i, cap(*arr))
	}
	f := func(slice *SliceArray) {
		slice.AddLast(34)
	}
	f(arr)
	fmt.Println(arr)
	fmt.Println(len(*arr))
	fmt.Println(cap(*arr))
	newSLice := (*arr)[1:]
	fmt.Println(newSLice)
	newSLice.AddLast(1)

	arr.AddLast(2)
	arr.AddLast(2)
	fmt.Println(arr)
	fmt.Println(newSLice)
}

func TestSliceArray_ToSlice(t *testing.T) {
	arr := CreateSliceArrayDefault()
	arr.AddLast(1, 2, 3, 4)
	slice := arr.ToSlice()
	t.Log(slice)
	slice[0] = -1
	t.Log(slice)
	t.Log(arr)
	slice = append(slice, 5)
	t.Log(slice)
	t.Log(arr)
	arr.AddLast(6)
	t.Log(slice)
	t.Log(arr)
}

func TestSliceArray_SelectionSort(t *testing.T) {
	arr := CreateSliceArrayDefault()
	arr.AddLast(5, 2, 3, 4)
	slice := arr.ToSlice()
	util.SortWithFunc(arr, true, func(thisValue interface{}, compareValue interface{}) int {
		i := thisValue.(int)
		j := compareValue.(int)
		if i == j {
			return 0
		} else if i > j {
			return -1
		} else {
			return 1
		}
	}, util.SelectionSort)
	t.Log(slice)
	t.Log(arr)
	var preValue interface{}
	for _, v := range *arr {
		if preValue != nil {
			if util.DefaultComparator(preValue, v) > 0 {
				t.Error("is sort error")
			}
		}
		preValue = v
	}
}

func TestSliceArray_InsertSort(t *testing.T) {
	arr := CreateSliceArrayDefault()
	arr.AddLast(5, 2, 3, 4)
	slice := arr.ToSlice()
	util.SortWithFunc(arr, false, nil, util.InsertSort)
	t.Log(slice)
	t.Log(arr)
	var preValue interface{}
	for _, v := range *arr {
		if preValue != nil {
			if util.DefaultComparator(preValue, v) > 0 {
				t.Error("is sort error")
			}
		}
		preValue = v
	}
}

func TestSliceArray_MergeSort(t *testing.T) {
	arr := CreateSliceArrayDefault()
	arr.AddLast(5, 2, 3, 4, 0)
	slice := arr.ToSlice()
	util.SortWithFunc(arr, false, nil, util.MergeSort)
	t.Log(slice)
	t.Log(arr)
	var preValue interface{}
	for _, v := range *arr {
		if preValue != nil {
			if util.DefaultComparator(preValue, v) > 0 {
				t.Error("is sort error")
			}
		}
		preValue = v
	}
}

func TestSliceArray_MergeSortBU(t *testing.T) {
	arr := CreateSliceArrayDefault()
	arr.AddLast(5, 2, 3, 4, 0)
	slice := arr.ToSlice()
	util.SortWithFunc(arr, false, nil, util.MergeSortBU)
	t.Log(slice)
	t.Log(arr)
	var preValue interface{}
	for _, v := range *arr {
		if preValue != nil {
			if util.DefaultComparator(preValue, v) > 0 {
				t.Error("is sort error")
			}
		}
		preValue = v
	}
}

func TestSliceArray_QuickSort(t *testing.T) {
	arr := CreateSliceArrayDefault()
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	slice := arr.ToSlice()
	util.SortWithFunc(arr, false, nil, util.QuickSort)
	t.Log(slice)
	t.Log(arr)
	var preValue interface{}
	for _, v := range *arr {
		if preValue != nil {
			if util.DefaultComparator(preValue, v) > 0 {
				t.Error("is sort error")
			}
		}
		preValue = v
	}
}

func TestSliceArray_HeapSort(t *testing.T) {
	arr := CreateSliceArrayDefault()
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	arr.AddLast(5, 2, 3, 4, 0)
	slice := arr.ToSlice()
	util.SortWithFunc(arr, true, nil, util.HeapSort)
	t.Log(slice)
	t.Log(arr)
	var preValue interface{}
	for _, v := range *arr {
		if preValue != nil {
			if util.DefaultComparator(preValue, v) < 0 {
				t.Error("is sort error")
			}
		}
		preValue = v
	}
}

func BenchmarkSliceArray_SelectionSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		arr := CreateSliceArrayDefault()
		for i := 0; i < 50000; i++ {
			arr.AddLast(rand.Int())
		}
		b.StartTimer()
		util.SortWithFunc(arr, false, nil, util.SelectionSort)
		b.StopTimer()
		var preValue interface{}
		for _, v := range *arr {
			if preValue != nil {
				if util.DefaultComparator(preValue, v) > 0 {
					b.Error("is sort error")
				}
			}
			preValue = v
		}
	}
}

/*
	is faster than SelectionSort
*/
func BenchmarkSliceArray_InsertSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		arr := CreateSliceArrayDefault()
		for i := 0; i < 50000; i++ {
			arr.AddLast(rand.Int())
		}
		b.StartTimer()
		util.SortWithFunc(arr, true, nil, util.InsertSort)
		b.StopTimer()
		var preValue interface{}
		for _, v := range *arr {
			if preValue != nil {
				if util.DefaultComparator(preValue, v) < 0 {
					b.Error("is sort error")
				}
			}
			preValue = v
		}
	}
}

func BenchmarkSliceArray_MergeSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		arr := CreateSliceArrayDefault()
		for i := 0; i < 50000; i++ {
			arr.AddLast(rand.Int())
		}
		b.StartTimer()
		util.SortWithFunc(arr, false, nil, util.MergeSort)
		b.StopTimer()
		var preValue interface{}
		for _, v := range *arr {
			if preValue != nil {
				if util.DefaultComparator(preValue, v) > 0 {
					b.Error("is sort error")
				}
			}
			preValue = v
		}
	}
}

func BenchmarkSliceArray_MergeSortBU(b *testing.B) {
	for i := 0; i < b.N; i++ {
		arr := CreateSliceArrayDefault()
		for i := 0; i < 50000; i++ {
			arr.AddLast(rand.Int())
		}
		b.StartTimer()
		util.SortWithFunc(arr, false, nil, util.MergeSortBU)
		b.StopTimer()
		var preValue interface{}
		for _, v := range *arr {
			if preValue != nil {
				if util.DefaultComparator(preValue, v) > 0 {
					b.Error("is sort error")
				}
			}
			preValue = v
		}
	}
}

func BenchmarkSliceArray_QuickSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		arr := CreateSliceArrayDefault()
		for i := 0; i < 50000; i++ {
			arr.AddLast(rand.Int())
		}
		b.StartTimer()
		util.SortWithFunc(arr, false, nil, util.QuickSort)
		b.StopTimer()
		var preValue interface{}
		for _, v := range *arr {
			if preValue != nil {
				if util.DefaultComparator(preValue, v) > 0 {
					b.Error("is sort error")
				}
			}
			preValue = v
		}
	}
}

func BenchmarkSliceArray_HeapSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		arr := CreateSliceArrayDefault()
		for i := 0; i < 50000; i++ {
			arr.AddLast(rand.Int())
		}
		b.StartTimer()
		util.SortWithFunc(arr, false, nil, util.HeapSort)
		b.StopTimer()
		var preValue interface{}
		for i, v := range *arr {
			if preValue != nil {
				if util.DefaultComparator(preValue, v) > 0 {
					b.Error("is sort error :", preValue, v, i)
				}
			}
			preValue = v
		}
	}
}

func TestCreateDynamicArrayDefault(t *testing.T) {
	arrayDefault := CreateDynamicArrayDefault()
	if arrayDefault.Size() != 0 {
		t.Error("CreateSliceArrayDefault excepted size is 0 but is", arrayDefault.Size())
	}
	if cap(arrayDefault.data) != 16 {
		t.Error("CreateSliceArrayDefault excepted cap is 16 but is", cap(arrayDefault.data))
	}
}

func TestCreateDynamicArray(t *testing.T) {
	arrayDefault := CreateDynamicArray(32)
	if arrayDefault.Size() != 0 {
		t.Error("CreateSliceArrayDefault excepted size is 0 but is", arrayDefault.Size())
	}
	if cap(arrayDefault.data) != 32 {
		t.Error("CreateSliceArrayDefault excepted cap is 32 but is", cap(arrayDefault.data))
	}
}

func TestDynamicArray_Add(t *testing.T) {
	arrayDefault := CreateDynamicArrayDefault()
	if arrayDefault.Size() != 0 {
		t.Error("CreateSliceArrayDefault excepted size is 0 but is", arrayDefault.Size())
	}
	if cap(arrayDefault.data) != 16 {
		t.Error("CreateSliceArrayDefault excepted cap is 16 but is", cap(arrayDefault.data))
	}
	arrayDefault.Add(0, 0)
	if arrayDefault.data[0] != 0 {
		t.Error("arrayDefault index of 0 data excepted is 0 but is", arrayDefault.data[0])
	}
	if "DynamicArray: size = 1 , capacity = 16 [0]" != arrayDefault.String() {
		t.Error("arrayDefault String is excepted is DynamicArray: size = 1 , capacity = 16 [1] but is ", arrayDefault.data[0])
	}
	for i := 1; i < 16; i++ {
		arrayDefault.Add(i, i)
	}
	for i := 0; i < arrayDefault.size; i++ {
		if arrayDefault.data[i] != i {
			t.Errorf("%d of v excepted is %v but is %v", i, i, arrayDefault.data[i])
		}
	}
	t.Log(arrayDefault.String())
	arrayDefault.Add(arrayDefault.Size(), 16)
	if arrayDefault.Size() != 17 || cap(arrayDefault.data) != 32 {
		t.Errorf("array excepted: size %v cap %v but size %v cap %v", 16, 32, arrayDefault.Size(), cap(arrayDefault.data))
	}
	t.Log(arrayDefault.String())
}

func TestDynamicArray_AddFirst(t *testing.T) {
	arrayDefault := CreateDynamicArrayDefault()
	if arrayDefault.Size() != 0 {
		t.Error("CreateSliceArrayDefault excepted size is 0 but is", arrayDefault.Size())
	}
	if cap(arrayDefault.data) != 16 {
		t.Error("CreateSliceArrayDefault excepted cap is 16 but is", cap(arrayDefault.data))
	}
	arrayDefault.Add(0, 1)
	if arrayDefault.data[0] != 1 {
		t.Error("arrayDefault index of 0 data excepted is 1 but is", arrayDefault.data[0])
	}
	arrayDefault.Add(0, 2)
	if arrayDefault.data[0] != 2 {
		t.Error("arrayDefault index of 0 data excepted is 2 but is", arrayDefault.data[0])
	}
	t.Log(arrayDefault.String())
}

func TestDynamicArray_Contains(t *testing.T) {
	arrayDefault := CreateDynamicArrayDefault()
	if arrayDefault.Size() != 0 {
		t.Error("CreateSliceArrayDefault excepted size is 0 but is", arrayDefault.Size())
	}
	if cap(arrayDefault.data) != 16 {
		t.Error("CreateSliceArrayDefault excepted cap is 16 but is", cap(arrayDefault.data))
	}
	arrayDefault.Add(0, 1)
	if arrayDefault.data[0] != 1 {
		t.Error("arrayDefault index of 0 data excepted is 1 but is", arrayDefault.data[0])
	}
	arrayDefault.Add(0, 2)
	if arrayDefault.data[0] != 2 {
		t.Error("arrayDefault index of 0 data excepted is 2 but is", arrayDefault.data[0])
	}

	if contains := arrayDefault.Contains(1); !contains {
		t.Errorf("excepted contains 1 but not contains")
	}

	if contains := arrayDefault.Contains(3); contains {
		t.Errorf("excepted not contains 3 but return contains")
	}
}

func TestDynamicArray_Remove(t *testing.T) {
	arrayDefault := CreateDynamicArrayDefault()
	if arrayDefault.Size() != 0 {
		t.Error("CreateSliceArrayDefault excepted size is 0 but is", arrayDefault.Size())
	}
	if cap(arrayDefault.data) != 16 {
		t.Error("CreateSliceArrayDefault excepted cap is 16 but is", cap(arrayDefault.data))
	}
	arrayDefault.Add(0, 0)
	if arrayDefault.data[0] != 0 {
		t.Error("arrayDefault index of 0 data excepted is 0 but is", arrayDefault.data[0])
	}
	if "DynamicArray: size = 1 , capacity = 16 [0]" != arrayDefault.String() {
		t.Error("arrayDefault String is excepted is DynamicArray: size = 1 , capacity = 16 [1] but is ", arrayDefault.data[0])
	}
	for i := 1; i < 16; i++ {
		arrayDefault.Add(i, i)
	}
	for i := 0; i < arrayDefault.size; i++ {
		if arrayDefault.data[i] != i {
			t.Errorf("%d of v excepted is %v but is %v", i, i, arrayDefault.data[i])
		}
	}
	arrayDefault.Add(arrayDefault.Size(), 16)
	if arrayDefault.Size() != 17 || cap(arrayDefault.data) != 32 {
		t.Errorf("array excepted: size %v cap %v but size %v cap %v", 16, 32, arrayDefault.Size(), cap(arrayDefault.data))
	}

	for i := 0; i < 17; i++ {
		remove, _ := arrayDefault.Remove(0)
		if remove != i {
			t.Errorf("excepted remove is %v but is %v", i, remove)
		}
		if arrayDefault.Size() != 17-i-1 {
			t.Errorf("excepted size is %v but is %v", 17-i-1, arrayDefault.Size())
		}
		if i == 12 && cap(arrayDefault.data) != 16 {
			t.Errorf("array capacity excepted is resize to 16 but is %v", cap(arrayDefault.data))
		}
	}
}

func TestDynamicArray_RemoveElement(t *testing.T) {
	arrayDefault := CreateDynamicArrayDefault()
	if arrayDefault.Size() != 0 {
		t.Error("CreateSliceArrayDefault excepted size is 0 but is", arrayDefault.Size())
	}
	if cap(arrayDefault.data) != 16 {
		t.Error("CreateSliceArrayDefault excepted cap is 16 but is", cap(arrayDefault.data))
	}
	arrayDefault.Add(0, 0)
	if arrayDefault.data[0] != 0 {
		t.Error("arrayDefault index of 0 data excepted is 0 but is", arrayDefault.data[0])
	}
	if "DynamicArray: size = 1 , capacity = 16 [0]" != arrayDefault.String() {
		t.Error("arrayDefault String is excepted is DynamicArray: size = 1 , capacity = 16 [1] but is ", arrayDefault.data[0])
	}
	for i := 1; i < 16; i++ {
		arrayDefault.Add(i, i)
	}
	for i := 0; i < arrayDefault.size; i++ {
		if arrayDefault.data[i] != i {
			t.Errorf("%d of v excepted is %v but is %v", i, i, arrayDefault.data[i])
		}
	}
	arrayDefault.Add(arrayDefault.Size(), 16)
	if arrayDefault.Size() != 17 || cap(arrayDefault.data) != 32 {
		t.Errorf("array excepted: size %v cap %v but size %v cap %v", 16, 32, arrayDefault.Size(), cap(arrayDefault.data))
	}

	for i := 0; i < 17; i++ {
		arrayDefault.RemoveElement(i)
		t.Log(arrayDefault.String())
	}

}

func TestDynamicArray_RemoveAllElement(t *testing.T) {
	arrayDefault := CreateDynamicArrayDefault()
	if arrayDefault.Size() != 0 {
		t.Error("CreateSliceArrayDefault excepted size is 0 but is", arrayDefault.Size())
	}
	if cap(arrayDefault.data) != 16 {
		t.Error("CreateSliceArrayDefault excepted cap is 16 but is", cap(arrayDefault.data))
	}
	arrayDefault.Add(0, 0)
	if arrayDefault.data[0] != 0 {
		t.Error("arrayDefault index of 0 data excepted is 0 but is", arrayDefault.data[0])
	}
	if "DynamicArray: size = 1 , capacity = 16 [0]" != arrayDefault.String() {
		t.Error("arrayDefault String is excepted is DynamicArray: size = 1 , capacity = 16 [1] but is ", arrayDefault.data[0])
	}
	for i := 1; i < 16; i++ {
		arrayDefault.Add(i, i)
	}
	for i := 0; i < arrayDefault.size; i++ {
		if arrayDefault.data[i] != i {
			t.Errorf("%d of v excepted is %v but is %v", i, i, arrayDefault.data[i])
		}
	}
	arrayDefault.Add(arrayDefault.Size(), 16)
	if arrayDefault.Size() != 17 || cap(arrayDefault.data) != 32 {
		t.Errorf("array excepted: size %v cap %v but size %v cap %v", 16, 32, arrayDefault.Size(), cap(arrayDefault.data))
	}
	for i := 0; i < 17; i++ {
		arrayDefault.Add(i, i)
	}
	for i := 0; i < 17; i++ {
		arrayDefault.RemoveAllElement(i)
		t.Log(arrayDefault.String())
	}

}

func TestDynamicArray_Iterator(t *testing.T) {
	arrayDefault := CreateDynamicArrayDefault()
	if arrayDefault.Size() != 0 {
		t.Error("CreateSliceArrayDefault excepted size is 0 but is", arrayDefault.Size())
	}
	if cap(arrayDefault.data) != 16 {
		t.Error("CreateSliceArrayDefault excepted cap is 16 but is", cap(arrayDefault.data))
	}
	arrayDefault.Add(0, 0)
	if arrayDefault.data[0] != 0 {
		t.Error("arrayDefault index of 0 data excepted is 0 but is", arrayDefault.data[0])
	}
	if "DynamicArray: size = 1 , capacity = 16 [0]" != arrayDefault.String() {
		t.Error("arrayDefault String is excepted is DynamicArray: size = 1 , capacity = 16 [1] but is ", arrayDefault.data[0])
	}
	for i := 1; i < 16; i++ {
		arrayDefault.Add(i, i)
	}
	for i := 0; i < arrayDefault.size; i++ {
		if arrayDefault.data[i] != i {
			t.Errorf("%d of v excepted is %v but is %v", i, i, arrayDefault.data[i])
		}
	}
	arrayDefault.Add(arrayDefault.Size(), 16)
	if arrayDefault.Size() != 17 || cap(arrayDefault.data) != 32 {
		t.Errorf("array excepted: size %v cap %v but size %v cap %v", 16, 32, arrayDefault.Size(), cap(arrayDefault.data))
	}
	for i := 0; i < 17; i++ {
		arrayDefault.Add(i, i)
	}
	for i := 0; i < 17; i++ {
		arrayDefault.Iterator(func(iterator common.Iterator) bool {
			if value, _ := iterator.Get(); value == i {
				iterator.Remove()
			}
			return false
		})
		t.Log(arrayDefault.String())
	}
}

func BenchmarkSliceArray_Add(b *testing.B) {
	for i := 0; i < b.N; i++ {
		arrayDefault := CreateSliceArrayDefault()
		for j := 0; j < 10000; j++ {
			arrayDefault.AddLast(1)
		}
	}
}

func BenchmarkSliceArray_RemoveLast(b *testing.B) {
	for i := 0; i < b.N; i++ {
		arrayDefault := CreateSliceArrayDefault()
		for j := 0; j < 10000; j++ {
			arrayDefault.AddLast(1)
		}
		b.StartTimer()
		for j := 0; j < 10000; j++ {
			arrayDefault.RemoveLast()
		}
		b.StopTimer()
	}
}

func BenchmarkDynamicArray_AddLast(b *testing.B) {
	for i := 0; i < b.N; i++ {
		arrayDefault := CreateDynamicArrayDefault()
		for j := 0; j < 10000; j++ {
			arrayDefault.AddLast(1)
		}
	}
}

func BenchmarkDynamicArray_RemoveLast(b *testing.B) {
	for i := 0; i < b.N; i++ {
		arrayDefault := CreateDynamicArrayDefault()
		for j := 0; j < 10000; j++ {
			arrayDefault.AddLast(1)
		}
		b.StartTimer()
		for j := 0; j < 10000; j++ {
			arrayDefault.RemoveLast()
		}
		b.StopTimer()
	}
}
